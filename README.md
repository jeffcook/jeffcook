# Jeff Cook

I'm an experienced Infrastructure Engineer with a focus on security and automation.

## Profiles

- [<img src="https://about.gitlab.com/images/press/logo/svg/gitlab-icon-rgb.svg" alt="GitLab" height="30"/> @jeffcook](https://gitlab.com/jeffcook)
- [<img src="https://github.githubassets.com/images/modules/logos_page/GitHub-Mark.png" alt="GitHub" height="30"/> @jeff-cook](https://github.com/jeff-cook)
- [<img src="https://content.linkedin.com/content/dam/me/business/en-us/amp/brand-site/v2/bg/LI-Bug.svg.original.svg" alt="LinkedIn" height="20"/> jeff--cook](https://www.linkedin.com/in/jeff--cook/)

